#ifdef DO_WHILE_FALSE
#define SCOPE_STARTS      do {
#define SCOPE_ESCAPE      break
#define SCOPE_FINISHES    } while(false);

#elif FOR_WHILE_NOT_DONE
#define SCOPE_STARTS      for (bool done = false; !done; done = true) {
#define SCOPE_ESCAPE      break
#define SCOPE_FINISHES    }

#elif WHILE_TRUE_WITH_BREAK
#define SCOPE_STARTS      while (true) {
#define SCOPE_ESCAPE      break
#define SCOPE_FINISHES    break; }

#elif SWITCH_TRUE
#define SCOPE_STARTS      switch (true) { default:
#define SCOPE_ESCAPE      break
#define SCOPE_FINISHES    }

#elif LAMBDA_RETURN
#define SCOPE_STARTS      []() {
#define SCOPE_ESCAPE      return
#define SCOPE_FINISHES    }();

#elif GOTO_FAIL
#define SCOPE_STARTS
#define SCOPE_ESCAPE      goto fail
#define SCOPE_FINISHES    fail:

#else
#warning Define some implementation.
#define SKIP_PROGRAM
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int , char** )
{
#ifndef SKIP_PROGRAM

// tag::main-code[]
    SCOPE_STARTS
        char* text = (char*)malloc(100);
        if (text == NULL)
            SCOPE_ESCAPE;
        text = strcpy(text, "hello world");
        puts(text);
        free(text);
    SCOPE_FINISHES
// end::main-code[]


#endif // SKIP_PROGRAM
    return 0;
}
