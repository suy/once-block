CXXFLAGS += -std=c++11 -Wall -O2

VARIANTS = DO_WHILE_FALSE FOR_WHILE_NOT_DONE WHILE_TRUE_WITH_BREAK SWITCH_TRUE LAMBDA_RETURN GOTO_FAIL

all: $(VARIANTS)

DO_WHILE_FALSE: main.cpp
	@echo Making $@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS)    main.cpp -o hello-$@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS) -S main.cpp -o hello-$@.S

FOR_WHILE_NOT_DONE: main.cpp
	@echo Making $@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS)    main.cpp -o hello-$@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS) -S main.cpp -o hello-$@.S

WHILE_TRUE_WITH_BREAK: main.cpp
	@echo Making $@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS)    main.cpp -o hello-$@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS) -S main.cpp -o hello-$@.S

SWITCH_TRUE: main.cpp
	@echo Making $@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS)    main.cpp -o hello-$@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS) -S main.cpp -o hello-$@.S

LAMBDA_RETURN: main.cpp
	@echo Making $@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS)    main.cpp -o hello-$@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS) -S main.cpp -o hello-$@.S

GOTO_FAIL: main.cpp
	@echo Making $@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS)    main.cpp -o hello-$@
	$(CXX) -D$@ $(CPPFLAGS) $(CXXFLAGS) -S main.cpp -o hello-$@.S

clean:
	-rm $(VARIANTS) hello-*

.PHONY: all clean
